# Test Kotlin Build Pipeline

Demo project to demonstrate how to leverage Gitlab's build pipelines for Kotlin.

## Demo

The following independent pull requests were created to show different states of the build pipeline.

- Broken test introduced. Test pipeline now fails: [PR](https://gitlab.com/org.hsmith.medium/kotlin-build-pipeline/-/merge_requests/1) ([pipeline](https://gitlab.com/org.hsmith.medium/kotlin-build-pipeline/pipelines/142273260))
- Fix code style errors. All steps now pass: [PR](https://gitlab.com/org.hsmith.medium/kotlin-build-pipeline/-/merge_requests/2) ([pipeline](https://gitlab.com/org.hsmith.medium/kotlin-build-pipeline/pipelines/142273459))


## Code

The code is very simple, it contains a calculator to add two numbers together.
There are two versions of this calculator: A working version (that calculates the sum correctly), and a broken version (that calculates the incorrect sum).

The main method just shows the output of both calculators.

The repository contains a simple test class to validate both calculators calculate the correct sum.
The test for the broken calculator has been disabled, but will fail if enabled.

## Gitlab CI

### General

Three stages:

```yaml
stages:
  - compile
  - test
  - package
```

Before each step executes set the `GRADLE_USER_HOME` variable to the `.gradle/` folder in the repository.

```yaml
before_script:
  - export GRADLE_USER_HOME=`pwd`/.gradle
```

Cache Gradle files between steps to avoid having to re-download them.

```yaml
cache:
  paths:
    - .gradle/wrapper
    - .gradle/caches
```

### Steps

All steps use the Gradle wrapper (`gradlew`) to run commands, this way Gradle does not need to be installed on the container to run the build process.

**Compile**

The `compile` step runs `gradle assemble` to ensure there are no build errors.

```yaml
compile:
  stage: compile
  script:
    - ./gradlew assemble
```

**Test**

The `test` step uses `gradle test` to run all tests configured in the project. This particular project uses JUnit5 as the test framework.

```yaml
test:
  stage: test
  script:
    - ./gradlew test --stacktrace
```

**Ktlint**

The `code_style` step uses [Ktlint](https://github.com/pinterest/ktlint) to check the code for linting errors.
I have configured Ktlint to generate a report file at `build/ktlint.xml`. This file will get uploaded by Gitlab during the build process and stored for up to 1 day.

I have also configured this step to allow failures. In other words if there are linting errors this step will show a warning, but the rest of the pipeline will continue.

```yaml
code_style:
  stage: test
  script:
    - ./gradlew ktlint
  artifacts:
    paths:
      - build/ktlint.xml
    expire_in: 1 day
  allow_failure: true
```

**Package**

Finally, the `package` step uses the [ShadowJar](https://github.com/johnrengelman/shadow) Gradle plugin to compile a "fat jar" with all the dependencies included.
This file then gets uploaded as an artifact of the build process and stored for 1 day.

```yaml
package:
  stage: package
  script:
    - ./gradlew shadowJar
  artifacts:
    paths:
      - build/libs/*.jar
    expire_in: 1 day
```

## Resources

- [Ktlint](https://github.com/pinterest/ktlint)
- [ShadowJar](https://github.com/johnrengelman/shadow)
